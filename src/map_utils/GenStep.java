/**
 * 
 */
package map_utils;

import java.util.Random;

/**
 * @author phind
 *
 */
public class GenStep {
	public static final int UP=1;
	public static final int DOWN=2;
	public static final int LEFT=3;
	public static final int RIGHT=4;
	private int distance;
	private int direct;
	private int maxX; 										//width
	private int maxY; 										//height
	
	
	public GenStep(int maxX, int maxY){
		this.maxX=maxX;
		this.maxY=maxY;
	}
	
	public void nextStep(int currentX, int currentY){
		Random r=new Random();
		direct = r.nextInt(3)+1;
		System.out.println(currentX+" : "+currentY);
		switch(direct){
		case UP:
			if(currentY<1){
				distance=0;
				return;
			}
			distance=r.nextInt(currentY)+1;
			break;
		case DOWN:
			if(maxY-currentY-1<1){
				distance=0;
				return;
			}
			distance=r.nextInt(maxY-currentY-1)+1;
			break;
		case LEFT:
			if(currentX<1){
				distance=0;
				return;
			}
			distance=r.nextInt(currentX)+1;
			break;
		case RIGHT:
			if(maxX-currentX-1<1){
				distance=0;
				return;
			}
			distance=r.nextInt(maxX-currentX-1)+1;
			break;
		default:
			//nothing to do here
			break;
		}
		System.out.println("move: "+direct+" : "+distance);
	}

	public int getDistance() {
		return distance;
	}

	public int getDirect() {
		return direct;
	}
	
	
}
