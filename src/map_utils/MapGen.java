/**
 * 
 */
package map_utils;

import java.util.Random;

/**
 * @author phind
 *
 */
public class MapGen {
	private int maxX;
	private int maxY;
	private int map[][];
	private int number;
	private int currentX;
	private int currentY;
	private GenStep genStep;
	
	public MapGen(int x, int y, int number) {
		Random r=new Random();
		maxX=x;
		maxY=y;
		this.number=number;
		map=new int[x][y];
		genStep=new GenStep(x, y);
		currentX=r.nextInt(x);
		currentY=r.nextInt(y);
		for(int i=0; i<x; i++){
			for(int j=0; j<y; j++){
				map[i][j]=0;
			}
		}
		map[currentX][currentY]=1;
	}
	
	public void genMap(){
		int i=0;
		while(i<number-1){
			genStep.nextStep(currentX, currentY);
			int direct=genStep.getDirect();
			int distance=genStep.getDistance();
			if(distance==0){
				continue;
			}
				
			switch(direct){
			case GenStep.UP:
				currentY-=distance;
				break;
			case GenStep.DOWN:
				currentY+=distance;
				break;
			case GenStep.LEFT:
				currentX-=distance;
				break;
			case GenStep.RIGHT:
				currentX+=distance;
				break;
			default:
					break;
			}
			map[currentX][currentY]=1;
			i+=1;
		}
	}
	
	public void print(){
		for(int i=0; i<maxX; i++){
			for(int j=0; j<maxY; j++){
				System.out.print(map[j][i]+"\t");
			}
			System.out.println("\n");
		}
	}
}
