using System;

namespace MapGenerator
{
	public class StepGenerator
	{
		private const int UP=0;
		private const int DOWN=1;
		private const int RIGHT=2;
		private const int LEFT=3;
		private int direct;
		private int distance;
		private int maxX;
		private int maxY;
		private Random randomGen;

		public StepGenerator ()
		{
		}

		public StepGenerator (int argMaxX, int argMaxY)
		{
			this.maxX = argMaxX;
			this.maxY = argMaxY;
			randomGen = new Random ();
		}

		public void nextStep (int currentX, int CurrentY)
		{
			direct = randomGen.Next (1, 80);
			direct %= 4;
			int temp;
			switch (direct) {
			case UP:
				if (CurrentY < 1) {
					distance = 0;
					return;
				}
				distance = randomGen.Next (1, CurrentY);
				break;
			case DOWN:
				if (maxY - CurrentY - 1 < 1) {
					distance = 0;
					return;
				}
				temp = maxY - CurrentY - 1;
				distance = randomGen.Next (1 + temp);
				break;
			case RIGHT:
				if (currentX < 1) {
					distance = 0;
					return;
				}
				distance = randomGen.Next (1, currentX);
				break;
			case LEFT:
				if (maxX - currentX - 1 < 1) {
					distance = 0;
					return;
				}
				temp = maxX - currentX - 1;
				distance = randomGen.Next (1, temp);
				break;
			default:
				break;
			}
		}

		/**
		 * Setter for direct and distance
		 * These number need to be return to the map generator
		 */
		public int Direct {
			get {
				return direct;
			}
		}

		public int Distance {
			get {
				return distance;
			}
		}
	}
}

