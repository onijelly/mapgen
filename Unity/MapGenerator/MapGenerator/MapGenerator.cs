using System;

namespace MapGenerator
{
	public class MapGenerator
	{
		private const int UP=0;
		private const int DOWN=1;
		private const int RIGHT=2;
		private const int LEFT=3;
		private int maxX;
		private int maxY;
		private int number;
		private int currentX;
		private int currentY;
		private int previous;
		private StepGenerator genStep;
		private Random ranGen;
		public int[,] map;
		//public
		public int heroX, heroY;

		public MapGenerator ()
		{
		}
		public MapGenerator(int x, int y, int number)
		{
			this.maxX = x;
			this.maxY = y;
			this.number = number;
			genStep = new StepGenerator (maxX, maxY);
			map = new int[maxX, maxY];
			ranGen = new Random ();
			currentX = ranGen.Next (0, maxX);
			currentY = ranGen.Next (0, maxY);
			for (int i=0; i< 30; i++) 
			{
				for (int j=0; j<30; j++) 
				{
					map [i, j] = 0;
				}
			}
			heroX = currentX;
			heroY = currentY;
			map [heroX, heroY] = 1;
			previous = 10;
		}

		public void genMap()
		{
			int i = 0, tempX, tempY;
			while (i<number) 
			{
				tempX = currentX;
				tempY = currentY;
				genStep.nextStep (currentX, currentY);
				int direct = genStep.Direct;
				int distance = genStep.Distance;
				if (distance == 0) 
				{
					continue;
				}
				switch (direct) 
				{
				case UP:
					tempY = tempY - distance;
					if (previous == DOWN) {
						continue;
					}
					break;
				case DOWN:
					tempY = tempY + distance;
					if (previous == UP) {
						continue;
					}
					break;
				case LEFT:
					tempX = currentX - distance;
					if (previous == RIGHT) {
						continue;
					}
					break;
				case RIGHT:
					tempX = currentX + distance;
					if (previous == LEFT) {
						continue;
					}
					break;
				default:
					break;

				}
				if (checkExist (tempX, tempY) == 1)
					continue;
				if ((currentX != tempX) && (currentY != tempY))
					continue;
				currentX = tempX;
				currentY = tempY;
				previous = direct;
				map [currentX, currentY] = 1;
				i += 1;
			}
		}
		public int checkExist(int x, int y)
		{
			if(map[x,y]==1)
				return 1;
			return 0;
		}
		public int checkRow(int p1, int p2, int c)
		{
			return 0;
		}
		public int checkCol(int p1, int p2, int r)
		{
			return 0;
		}

		public int MaxX {
			get {
				return maxX;
			}
		}

		public int MaxY {
			get {
				return maxY;
			}
		}
	}
}

